using DemoInjection.DI;
using DemoInjection.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DemoInjection
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            ServiceCollection services = new ServiceCollection();
            ConfigureService(services);
            ServiceResolver resolver = new ServiceResolver(services.BuildServiceProvider());

            //Pour la Web Api
            GlobalConfiguration.Configuration.DependencyResolver = resolver;

            //Pour l'application ASP
            //DependencyResolver.SetResolver(resolver);
            ControllerBuilder.Current.SetControllerFactory(typeof(DIControllerFactory));
        }

        private void ConfigureService(IServiceCollection services)
        {
            services.AddControllers();
            services.AddTransient<ServiceA>();
            services.AddTransient<ServiceB>();
        }
    }
}
