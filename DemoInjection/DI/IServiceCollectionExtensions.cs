﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace DemoInjection.DI
{
    public static class IServiceCollectionExtensions
    {
        public static void AddControllers(this IServiceCollection services)
        {
            Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.Name.EndsWith("Controller"))
                .ToList().ForEach(t => services.AddTransient(t));
        }
    }
}