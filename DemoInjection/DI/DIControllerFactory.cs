﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Mvc;
using System.Web.Routing;

namespace DemoInjection.DI
{
    public class DIControllerFactory : DefaultControllerFactory
    {
        private IDependencyScope _scope;

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            _scope = GlobalConfiguration.Configuration.DependencyResolver.BeginScope();
            return (IController)_scope.GetService(controllerType);
        }

        public override void ReleaseController(IController controller)
        {
            base.ReleaseController(controller);
            _scope.Dispose();
        }
    }
}