﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;

namespace DemoInjection.DI
{

    // c'est un peu embêtant car 
    // pour la WEB api le resolver doit implémenter System.Web.Http.Dependencies.DependencyResolver;
    // et pour l'application ASP System.Web.Mvc.DependencyResolver
    public class ServiceResolver : IDependencyResolver, System.Web.Mvc.IDependencyResolver
    {
        private readonly IServiceProvider _provider;

        public ServiceResolver(IServiceProvider provider)
        {
            _provider = provider;
        }

        public IDependencyScope BeginScope()
        {
            return new ServiceResolver(_provider.CreateScope().ServiceProvider);
        }

        public void Dispose()
        {
            (_provider as IDisposable)?.Dispose();
        }

        public object GetService(Type serviceType)
        {
            return _provider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _provider.GetServices(serviceType);
        }
    }
}