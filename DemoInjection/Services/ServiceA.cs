﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace DemoInjection.Services
{
    public class ServiceA : IDisposable
    {
        public string Hello()
        {
            return "Hello";
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                Debug.WriteLine("Service A disposed");
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private bool _disposed = false;

    }
}