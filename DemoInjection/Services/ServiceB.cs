﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoInjection.Services
{
    public class ServiceB : IDisposable
    {
        private readonly ServiceA _serviceA;

        public ServiceB(ServiceA serviceA)
        {
            _serviceA = serviceA;
        }

        public void Dispose()
        {
            _serviceA.Dispose();
        }

        public string HelloWorld()
        {
            return _serviceA.Hello() + " World!";
        }
    }
}