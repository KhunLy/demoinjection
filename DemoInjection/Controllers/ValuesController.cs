﻿using DemoInjection.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DemoInjection.Controllers
{
    public class ValuesController : ApiController
    {
        private readonly ServiceB _serviceB;

        public ValuesController(ServiceB serviceB)
        {
            _serviceB = serviceB;
        }

        // GET api/values
        public string Get()
        {
            return _serviceB.HelloWorld();
        }
    }
}
