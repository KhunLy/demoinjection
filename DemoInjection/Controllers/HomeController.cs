﻿using DemoInjection.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoInjection.Controllers
{
    public class HomeController : Controller
    {
        private readonly ServiceB _serviceB;

        public HomeController(ServiceB serviceB)
        {
            _serviceB = serviceB;
        }

        public ActionResult Index()
        {
            ViewBag.Data = _serviceB.HelloWorld();
            return View(); ;
        }
    }
}
